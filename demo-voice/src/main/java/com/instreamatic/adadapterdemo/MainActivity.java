package com.instreamatic.adadapterdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

import com.google.android.material.snackbar.Snackbar;
import com.instreamatic.adman.Adman;
import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.Region;
import com.instreamatic.adman.Type;
import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.module.wphrase.WPhraseModule;
import com.instreamatic.adman.module.wphrase.WphraseEvent;
import com.instreamatic.adman.view.generic.DefaultAdmanView;
import com.instreamatic.adman.voice.AdmanVoice;
import com.instreamatic.embedded.core.LanguageCode;
import com.instreamatic.embedded.recognition.VoiceRecognitionEmbedded;

public class MainActivity extends AppCompatActivity implements WphraseEvent.Listener, AdmanEvent.Listener {

    final private static String TAG = "DemoVoice";

    private IAdman adman;

    private View startView;
    private View loadingView;
    private CheckBox chbWakePhrase;
    private View wakePhraseView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chbWakePhrase = (CheckBox)findViewById(R.id.chbWakePhrase);
        wakePhraseView = findViewById(R.id.wakePhrase);
        startView = findViewById(R.id.start);
        loadingView = findViewById(R.id.loading);
        startView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdmanRequest.Builder request_builder = getAdmanRequest();
                adman.updateRequest(request_builder, true);
                adman.start();
            }
        });

        checkPermission();
        initAdman();
    }

    @Override
    protected void onPause() {
        super.onPause();
        adman.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adman.play();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onWphraseEvent(WphraseEvent event) {
        WphraseEvent.Type type = event.getType();
        if (type == WphraseEvent.Type.START_DETECT || type == WphraseEvent.Type.STOP_DETECT) {
            runOnUiThread(new Runnable() {
                public void run() {
                    Log.d(TAG, "onWphraseEvent, type: " + type);
                    wakePhraseView.setVisibility(type == WphraseEvent.Type.START_DETECT ? View.VISIBLE : View.GONE);
                }
            });
        }
    }
    @Override
    public void onAdmanEvent(final AdmanEvent event) {
        Log.d(TAG, "onAdmanEvent: " + event.getType().name());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (event.getType()) {
                    case PREPARE:
                        startView.setVisibility(View.GONE);
                        loadingView.setVisibility(View.VISIBLE);
                        break;
                    case NONE:
                    case FAILED:
                    case SKIPPED:
                    case COMPLETED:
                        startView.setVisibility(View.VISIBLE);
                        loadingView.setVisibility(View.GONE);
                        break;
                    case STARTED:
                        loadingView.setVisibility(View.GONE);
                        break;
                }
            }
        });
    }

    private void initAdman() {
        AdmanRequest.Builder request_builder = getAdmanRequest();
        adman = new Adman(this, request_builder.build());
        adman.bindModule(new DefaultAdmanView(this));

        AdmanVoice admanVoice = new AdmanVoice(this);
        //for recognition on the device
        VoiceRecognitionEmbedded voiceRecognition = new VoiceRecognitionEmbedded(this);
        voiceRecognition.addModel("assets/models/large_instrmtc_ovr25_enUS.snsr", LanguageCode.ENGLISH_US);
        admanVoice.setVoiceRecognition(voiceRecognition);
        adman.bindModule(admanVoice);

        adman.addListener(this);
        initWphrase();
    }

    private void initWphrase() {
        String pathModelPhraseSpot
                //= "assets/models/spot-hbg-enUS-1.4.0-m.snsr";    // Hello Blue Genie
                //= "assets/models/spot_hey_radio_enUS.snsr";      // Hey, radio; Hey, iheart
                = "assets/models/spot_hey_listen_enUS.snsr";       // Hey, listen
        WPhraseModule wphraseModule = new WPhraseModule(this);
        wphraseModule.addListener(this);
        wphraseModule.addModel(pathModelPhraseSpot, LanguageCode.ENGLISH_US);
        adman.bindModule(wphraseModule);
    }

    private AdmanRequest.Builder getAdmanRequest() {
        boolean isPhraseSpot = chbWakePhrase.isChecked();
        AdmanRequest.Builder request_builder = null;
        if (isPhraseSpot) {
            request_builder = new AdmanRequest.Builder().setAdUrlAPI("https://x3.instreamatic.com/v5/vast/1976?microphone=1&type=vor");
        }
        else {
            request_builder = new AdmanRequest.Builder()
                    .setSiteId(1249)
                    .setRegion(Region.GLOBAL)
                    .setType(Type.VOICE);
        }
        return request_builder;
    }

    /**
     * Mic access
     **/
    final private static int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;
    private void checkPermission() {
        View mLayout = (View) findViewById(R.id.main_layout);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECORD_AUDIO)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                try {
                    Snackbar.make(mLayout, R.string.permission_record_audio_rationale,
                            Snackbar.LENGTH_INDEFINITE)
                            .setAction(R.string.ok, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ActivityCompat.requestPermissions(MainActivity.this,
                                            new String[]{Manifest.permission.RECORD_AUDIO},
                                            PERMISSIONS_REQUEST_RECORD_AUDIO);
                                }
                            })
                            .show();
                } catch (Exception ex) {

                }
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        PERMISSIONS_REQUEST_RECORD_AUDIO);

                // PERMISSIONS_REQUEST_RECORD_AUDIO is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}